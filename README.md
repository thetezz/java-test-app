#java-test-app

to build the app
$ mvn package

to build the image
$ docker build --build-arg JAR_FILE=./target/java-test-app-0.0.1-SNAPSHOT.jar --rm -t java-test-app:latest . -f Dockerfile

to run the image
$ docker run -d --name=java-test-app -p 8080:8080 java-test-app
