FROM alpine:3.14

RUN  apk update \
  && apk upgrade \
  && apk add --update openjdk11 tzdata curl unzip bash \
  && rm -rf /var/cache/apk/*

ARG JAR_FILE

RUN mkdir -p /app1

COPY ${JAR_FILE} /app1/app.jar

EXPOSE 8080

CMD ["java", "-jar", "/app1/app.jar"]
