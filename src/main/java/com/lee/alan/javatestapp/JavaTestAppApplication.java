package com.lee.alan.javatestapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaTestAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaTestAppApplication.class, args);
	}

}
